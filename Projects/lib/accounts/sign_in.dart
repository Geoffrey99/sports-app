import 'package:flutter/material.dart';
import 'package:sports/ReusableWIdget/colors.dart';
import 'package:sports/ReusableWIdget/re_usable.dart';
import 'package:sports/accounts//signup.dart';
import 'package:sports/accounts/home.dart';
// import 'dart:js';

class SignInPage extends StatefulWidget {
const SignInPage({super.key});
@override
_SignInPageState createState() => _SignInPageState();
}
class _SignInPageState extends State<SignInPage> {
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _emailTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    // return const Scaffold();
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            hexStringToColors("CB2B19"),
            hexStringToColors("9546c4"),
            hexStringToColors("5E52F2")
          ],
            begin: Alignment.topCenter, end: Alignment.bottomCenter,
          ),
        ),

        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.fromLTRB(20, MediaQuery.of(context).size.height * 0.2, 20, 0),
            child: Column(
              children: [
                Center(child: logoWidget("assets/images/bird.jpg"),),
                SizedBox(
                  height: 30,
                ),
                reusableTextField("Enter Username", Icons.person_outline, false, _emailTextController),
                SizedBox(
                  height: 20,
                ),
                reusableTextField("Enter Password", Icons.lock_outline, true, _passwordController),
                SizedBox(
                  height: 20,
                ),
                reusableButtons(context, true, (){
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context)=>homePage()));
                }),
                login(),//BUTTONS
              ],
            ),
        ),
        ),
      ),
    );
  }

  Row login (){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text("Don't have Account?     ",
            style: TextStyle(color: Colors.white70)),
        GestureDetector(
          onTap: (){
            Navigator.push(context,
            MaterialPageRoute(builder: (context)=> signUp()));
                },
           child: Text("SIGN UP",
             style: TextStyle(
                 color: Colors.white,
                 fontWeight: FontWeight.bold),
           ),
            ),
      ],
    );
  }
}