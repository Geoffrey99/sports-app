import 'package:flutter/material.dart';
import 'package:sports/ReusableWIdget/colors.dart';
import 'package:sports/ReusableWIdget/re_usable.dart';
import 'package:sports/accounts/home.dart';

class signUp extends StatefulWidget {
  const signUp({super.key});

  @override
  State<signUp> createState() => _signUpState();
}
class _signUpState extends State<signUp> {
  TextEditingController _passwordController1 = TextEditingController();
  TextEditingController _passwordController2 = TextEditingController();
  TextEditingController _emailTextController = TextEditingController();
  TextEditingController _userNameController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text("Sign Up",
          style: TextStyle(
              color: Colors.white70,
              fontWeight: FontWeight.bold,
              fontSize: 24),
        ),
      ),
      
      body: Container(
        decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
        hexStringToColors("CB2B19"),
        hexStringToColors("9546c4"),
        hexStringToColors("5E52F2")],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter),
        ),

        child: SingleChildScrollView(
          child: Padding(
            // padding: EdgeInsets.fromLTRB(20, MediaQuery.of(context).size.height * 0.2, 20, 0),
            padding: EdgeInsets.fromLTRB(20, 120, 20, 0),

            child: Column(
              children: [
                Center(child: logoWidget("assets/images/bird.jpg"),),
                SizedBox(
                  height: 30,
                ),
                reusableTextField("Enter Username ",
                    Icons.person_outline, false,
                    _userNameController),
                SizedBox(
                  height: 30,
                ),
                reusableTextField("Enter Email ",
                    Icons.person_outline, false,
                    _emailTextController),
                SizedBox(
                  height: 20,
                ),
                reusableTextField("Enter Password ",
                    Icons.lock_outline, true,
                    _passwordController1),
                SizedBox(
                  height: 20,
                ),
                reusableTextField("Confirm Password ",
                    Icons.lock_outline, true,
                    _passwordController2),
                SizedBox(
                  height: 20,
                ),
                reusableButtons(context, false, (){
                  Navigator.push(context,
                  MaterialPageRoute(builder: (context)=>homePage()));
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
