import 'package:flutter/material.dart';
import 'package:sports/accounts/sign_in.dart';
// import 'package:sports/accounts/signup.dart';

class homePage extends StatefulWidget {
  const homePage({super.key});

  @override
  State<homePage> createState() => _homePageState();
}

class _homePageState extends State<homePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(
          child: Text("Log Out"),
          onPressed: (){
            Navigator.push(context,
              MaterialPageRoute(builder: (context)=> SignInPage()));
          },
        ),

      ),
    );
  }
}


//BACKGROUND COLORS
//decoration: BoxDecoration(
//           gradient: LinearGradient(colors: [
//             hexStringToColors("CB2B19"),
//             hexStringToColors("9546c4"),
//             hexStringToColors("5E52F2")
//           ],
//             begin: Alignment.topCenter, end: Alignment.bottomCenter,
//           ),
//         ),