import 'package:flutter/material.dart';
import 'accounts/sign_in.dart';

void main() => runApp(HomePage());

class HomePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey,
          title: Text("FLUTTER AUTHENTICATION", textAlign: TextAlign.center),
        ),
        body: SignInPage(),
      ),

    );

  }
}


